import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_issue/viewModel.dart';

class EditItem extends ConsumerWidget {
  const EditItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final vMstate = ref.watch(viewModelProv);
    bool saving = vMstate == 1 ? true : false;
    final item = ref.watch(viewModelProv.notifier).item;
    return Scaffold(
      appBar: AppBar(title: Text('Showing item')),
      body:  saving
            ? Center( child:CircularProgressIndicator())
            : Column(
        mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(child: Text(item)),
                  ElevatedButton(
                      onPressed: () {
                        ref
                            .read(viewModelProv.notifier)
                            .updateItem('Update some properties');
                      },
                      child: Text('Update'))
                ],
              ),
    );
  }
}
