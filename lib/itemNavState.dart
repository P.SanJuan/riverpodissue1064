import 'package:flutter_riverpod/flutter_riverpod.dart';


final navProv = StateNotifierProvider<ItemNav,String>((ref) {
  return ItemNav();
});



class ItemNav extends StateNotifier<String> {
  ItemNav() : super('');
  

  void viewItem({String? item,bool pageActive:true}){
    if(pageActive) {
      state = item!;
    }
    else
      state = '';
  }


}
