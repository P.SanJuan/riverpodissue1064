import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'itemNavState.dart';

final viewModelProv =
StateNotifierProvider.autoDispose<ViewModel, int>((ref) {
  final navState = ref.watch(navProv);

  final notifier = ref.watch(navProv.notifier);
  final item = navState.toString();
  ref.onDispose(() {
     print("disposing viewModelProv");
     //ref.watch(navProv.notifier).viewItem(pageActive: false);
    notifier.viewItem(pageActive: false);
  });

  return ViewModel(ref.read, item);
});

class ViewModel extends StateNotifier<int> {
  final Reader _read;
  final String _item;
  String get item => _item;


  ViewModel(this._read, this._item) : super(0);

  Future<void> updateItem(String update) async {
    state = 1;
      // DO Database stuff
    await Future.delayed(Duration(seconds: 5));
    state = 0;
    }
  }
